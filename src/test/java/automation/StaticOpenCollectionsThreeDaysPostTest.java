package automation;

import foodsharing.*;
import foodsharingpage.*;
import helper.*;
import model.*;
import org.junit.*;

import java.io.*;

public class StaticOpenCollectionsThreeDaysPostTest extends PageTest {

    private static final BusinessesAndDates businesses_and_dates = new BusinessesAndDates();

    @Test
    public void daily_task_post_open_collections() throws IOException {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        System.out.println("::: LoginPage loaded.");
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);
        System.out.println("::: DashBoard loaded.");

        for (String url : Read.urls_from(R.path_to_businesses_url_file)) {
            BusinessPage businessPage = Get.business_page(login, url);
            assertCorrectPage(url, businessPage);
            Business business = businessPage.convert_to_business();
            businesses_and_dates.add(business);
            System.out.println("::: BusinessPage loaded " + business.name());
        }

        ThreadPage threadPage = Get.threadpage(login);
        assertCorrectPage(threadPage);
        System.out.println("::: ThreadPage loaded.");
        threadPage.delete_all_but_first_thread();
        threadPage.post(businesses_and_dates.to_forum_thread());
        System.out.println("::: " + businesses_and_dates.to_forum_thread());

        close(threadPage);
    }
}