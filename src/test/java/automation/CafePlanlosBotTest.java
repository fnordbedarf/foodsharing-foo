package automation;

import org.junit.*;
import org.telegram.telegrambots.*;
import org.telegram.telegrambots.meta.*;
import org.telegram.telegrambots.meta.exceptions.*;
import org.telegram.telegrambots.meta.generics.*;
import telegram.*;

public class CafePlanlosBotTest {

    public static void main(String[] args) {
        CafePlanlosBotTest test = new CafePlanlosBotTest();
        test.run_bot_for_a_while();
    }

    @Test
    public void run_bot_for_a_while() {
        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();
        try {
            BotSession session = botsApi.registerBot(new CafePlanlosBot());
            while (session.isRunning()) {
                ; // TODO: Make this more elegant.
            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}