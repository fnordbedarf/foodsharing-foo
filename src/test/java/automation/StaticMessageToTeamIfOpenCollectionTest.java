package automation;

import foodsharing.*;
import foodsharingpage.*;
import helper.*;
import org.junit.*;

import java.io.*;

public class StaticMessageToTeamIfOpenCollectionTest extends PageTest {
    // TODO Die nachrichten vielleicht noch abwechslungsreicher gestalten

    @Test
    public void daily_task_post_open_collections() throws IOException {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);

        BusinessPage businessPage = null;

        for (String url : Read.urls_from(R.path_to_businesses_to_be_messages_url_file)) {
            businessPage = Get.business_page(login, url);
            assertCorrectPage(url, businessPage);

            businessPage.message_to_team_if_open_collections_exist();
        }

        close(businessPage);
    }
}