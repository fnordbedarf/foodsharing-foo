package automation;

import foodsharing.*;
import foodsharingpage.*;
import helper.*;
import org.junit.*;

import java.io.*;

public class StaticConfirmCollectorTest extends PageTest {

    private static BusinessPage businessPage;

    @Test
    public void hourly_confirm_all_collectors() throws IOException {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);

        for (String url : Read.urls_from(R.path_to_businesses_url_to_confirm_collectors_file)) {
            businessPage = Get.business_page(dashboard, url);
            assertCorrectPage(businessPage);
            businessPage.confirm_all_collectors();
        }

        close(businessPage);
    }
}