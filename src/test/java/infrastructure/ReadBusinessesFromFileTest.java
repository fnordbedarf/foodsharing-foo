package infrastructure;

import helper.*;
import org.junit.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import static org.junit.Assert.*;

public class ReadBusinessesFromFileTest {

    private static final List<String> urls = new LinkedList<>();
    private static final String filename = R.path_to_businesses_url_file;

    @Before
    public void before() {
        try {
            Stream<String> lines = Files.lines(Paths.get(filename));
            lines.forEach(urls :: add);
            lines.close();
        } catch(IOException io) {
            io.printStackTrace();
        }
    }

    @Test
    public void read_business_from_file_version_1() throws IOException {
        List<String> actual = Read.urls_from(filename);
        assertEquals(urls, actual);
    }

    @Test
    public void read_business_from_file() {
        System.out.println(urls);
    }
}