package infrastructure;

import helper.*;
import org.junit.*;
import org.openqa.selenium.*;

import static junit.framework.TestCase.*;

public class WebDriverExceptionTest {

    private static WrappedWebDriverException wrapper = null;

    @Test
    public void test() {
        try {
            WebDriverException e = new WebDriverException();
            e.addInfo("hello", "world");
            throw e;
        } catch ( WebDriverException e) {
            wrapper = new WrappedWebDriverException(e);
        }
        assertTrue(wrapper.toString().contains("hello: world"));
    }
}
