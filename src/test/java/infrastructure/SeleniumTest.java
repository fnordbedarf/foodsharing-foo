package infrastructure;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;

import static org.junit.Assert.*;

public class SeleniumTest {

    @Test
    public void start_selenium() {
        String path = "/home/sammann/git/foodsharing-foo/src/test/resources/geckodriver";
        System.setProperty("webdriver.gecko.helper", path);
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.com");
        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("Cheese!");
        element.submit();
        assertEquals("Google", driver.getTitle());
        driver.quit();
    }
}
