package infrastructure;

import foodsharing.*;
import foodsharingpage.*;
import helper.*;
import org.junit.*;
import org.telegram.telegrambots.*;
import org.telegram.telegrambots.meta.*;
import org.telegram.telegrambots.meta.api.methods.send.*;
import org.telegram.telegrambots.meta.exceptions.*;
import telegram.*;

import java.io.*;

public class TelegramBotTest extends PageTest  {

    private static BusinessPage businessPage;
    private static CafePlanlosBot bot = new CafePlanlosBot();

    @Before
    public void before() {
        ApiContextInitializer.init();

        TelegramBotsApi botsApi = new TelegramBotsApi();

        try {
            botsApi.registerBot(bot);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void message_confirmation_successful() throws IOException, TelegramApiException {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);

        for (String url : Read.urls_from(R.path_to_businesses_url_to_confirm_collectors_file)) {
            businessPage = Get.business_page(dashboard, url);
            assertCorrectPage(businessPage);
            businessPage.confirm_all_collectors();

            SendMessage message = new SendMessage() // Create a SendMessage object with mandatory fields
                    .setChatId("1")
                    .setText(url);
            try {
                bot.execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

        close(businessPage);



    }
}
