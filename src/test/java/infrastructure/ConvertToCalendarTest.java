package infrastructure;

import helper.*;
import org.junit.*;

import java.text.*;
import java.util.*;

import static org.junit.Assert.*;

public class ConvertToCalendarTest {

    private static final String timestamp = "2019-01-16 18:30:00";
    private static final SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Test
    public void german_format() throws ParseException {
        Calendar actual = To.calendar_convert(timestamp);
        String formatted = To.human_readable_string_convert(actual);
        System.out.println(formatted);
    }

    @Test
    public void test1() throws ParseException {
        Calendar actual1 = To.calendar_convert(timestamp);
        Calendar actual2 = Calendar.getInstance();
        actual2.setTime(dateformat.parse(timestamp));
        assertEquals(actual1, actual2);
    }

    @Test
    public void convert_calendar_to_string() throws ParseException {
        Calendar actual = To.calendar_convert(timestamp);
        String formatted = To.string_convert(actual);
        assertEquals(timestamp, formatted);
    }

    @Test
    public void convert_string_to_calendar() throws ParseException {
        Calendar actual = Calendar.getInstance();
        actual.setTime(dateformat.parse(timestamp));
        String formatted = dateformat.format(actual.getTime());
        assertEquals(timestamp, formatted);
    }
}