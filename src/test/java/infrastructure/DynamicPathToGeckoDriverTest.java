package infrastructure;

import helper.*;
import org.junit.*;

import java.io.*;

import static junit.framework.TestCase.*;

public class DynamicPathToGeckoDriverTest {

    @Test
    public void create_path_to_resource_version_1() {
        String path = R.path_to_webdriver_gecko_driver;
        System.out.println(path);
        File file = new File(path);
        assertTrue(file.exists());
    }

    @Test
    public void create_path_to_resource() {
        String to_project_folder = new File("").getAbsolutePath();
        String to_webdriver = R.path_to_webdriver_gecko_driver;
        File file = new File(to_project_folder + to_webdriver);
        assertTrue(file.exists());
    }
}
