package infrastructure;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import pageobject.*;

import java.util.*;

import static org.junit.Assert.*;

public class PropertiesFileTest {

    @Test
    public void get_password_from_file() {
        ResourceBundle properties = ResourceBundle.getBundle("login");
        assertEquals("e.shure@transition-siegen.de", properties.getString("email"));
    }
}