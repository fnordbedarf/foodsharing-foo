package infrastructure;

import helper.*;
import org.junit.*;

import static junit.framework.TestCase.*;

public class UTFResourceTest {

    @Test
    public void test () {
        String message = R.message_of(1);
        assertTrue(message.contains("ü"));
    }
}
