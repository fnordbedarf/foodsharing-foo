package foodsharing;

import foodsharingpage.*;
import helper.*;
import org.openqa.selenium.*;
import pageobject.*;

import static org.junit.Assert.*;

public class PageTest {

    protected static PageObject login() {
        PageTest pageTest = new PageTest();
        LoginPage login = null;
        boolean done = false;
        while ( ! done) {
            login = Get.login_page();
            pageTest.assertCorrectPage(login);
            login.with_credentials(R.email, R.password);
            DashBoardPage dashboard = Get.dashboard_page(login);
            pageTest.assertCorrectPage(dashboard);
            done = dashboard.verify();
            if ( ! done) {
                login.driver().quit();
            }
        }
        return login;
    }

    public void assertCorrectPage(PageObject object) {
        assertCorrectPage("", object);
    }

    public void assertCorrectPage(String message, PageObject object) {
        assertTrue(message, object.verify());
    }

    void close(WebDriver driver) {
        if (driver != null) driver.quit();
    }

    protected void close(PageObject page) {
        if (page != null) {
            page.wait_until_loaded(); // TODO wait until driver finished loading, not page
            close(page.driver());
        }
    }
}