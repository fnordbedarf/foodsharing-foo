package foodsharing;

import foodsharingpage.*;
import helper.*;
import model.*;
import org.junit.*;

import java.io.*;
import java.util.*;

public class GetOpenCollectionDatesForTheNextThreeDaysStaticTest extends PageTest {

    @Test
    public void get_all_businesses_urls_static_urls() throws IOException {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);
        dashboard.get(R.url_of_all_businesses_page);

        BusinessesAndDates map = new BusinessesAndDates();
        List<String> urls = Read.urls_from(R.path_to_businesses_url_file);

        for (String url : urls) {
            BusinessPage businessPage = Get.business_page(login, url);
            Business business = businessPage.convert_to_business();
            map.add(business);
        }

        String message = Print.overview_of_open(map);
        System.out.println(message);
    }
}