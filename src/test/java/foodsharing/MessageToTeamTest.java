package foodsharing;

import foodsharingpage.*;
import helper.*;
import org.junit.*;

public class MessageToTeamTest extends PageTest {

    @Test
    public void message_to_team() {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);

        BusinessPage businessPage = Get.business_page(dashboard, R.url_of_a_business);
        assertCorrectPage(businessPage);

        businessPage.message_to_team("test");

        close(businessPage);
    }
}
