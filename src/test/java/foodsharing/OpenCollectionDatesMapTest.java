package foodsharing;

import foodsharingpage.*;
import helper.*;
import model.*;
import org.junit.*;

import static org.junit.Assert.*;

public class OpenCollectionDatesMapTest extends PageTest{

    @Test
    public void add_to_businesses_to_map() {
        BusinessPage business_page = Get.business_page(PageTest.login(), R.url_of_a_business);
        assertCorrectPage(business_page);
        Business a_business = business_page.convert_to_business();
        assertNotNull(a_business.open_collection_dates());

        business_page = Get.business_page(business_page, R.url_of_another_business);
        assertCorrectPage(business_page);
        Business another_business = business_page.convert_to_business();
        assertNotNull(another_business.open_collection_dates());

        BusinessesAndDates map = new BusinessesAndDates();
        map.add(a_business);
        map.add(another_business);

        int sum = a_business.open_collection_dates().size() + another_business.open_collection_dates().size();
        int map_size = map.size();
        assertTrue(map_size <= sum);

        System.out.println(map);
    }
}