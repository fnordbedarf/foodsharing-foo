package foodsharing;

import foodsharingpage.*;
import helper.*;
import model.*;
import org.junit.*;

import java.util.*;

public class GetOpenCollectionDatesForTheNextThreeDaysTest extends PageTest {

    @Test
    public void get_all_businesses_urls() {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);
        dashboard.get(R.url_of_all_businesses_page);

        AllBusinessesPage all_businesses = Get.all_businesses_page(dashboard);
        List<String> urls = all_businesses.in_cooperation_urls();

        BusinessesAndDates map = new BusinessesAndDates();
        for (String url : urls) {
            BusinessPage businessPage = Get.business_page(login, url);
            Business business = businessPage.convert_to_business();
            map.add(business);
        }

        String message = Print.overview_of_open(map);
        System.out.println(message);
    }
}
