package foodsharing;

import foodsharingpage.*;
import helper.*;
import org.junit.*;

public class PostToThreadTest extends PageTest {

    private static final String url_of_thread = "https://foodsharing.de/?page=bezirk&bid=139&sub=forum&tid=6458";

    @Test
    public void post_to_thread() {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);

        ThreadPage threadPage = Get.threadpage(url_of_thread, login);
        assertCorrectPage(threadPage);

        threadPage.delete_all_but_first_thread();

        threadPage.post("message " + Get.timestamp());
    }
}