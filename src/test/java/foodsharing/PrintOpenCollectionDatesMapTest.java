package foodsharing;

import foodsharingpage.*;
import helper.*;
import model.*;
import org.junit.*;

import static org.junit.Assert.*;

public class PrintOpenCollectionDatesMapTest extends PageTest{

    BusinessesAndDates map;

    @Before
    public void before() {
        BusinessPage business_page = Get.business_page(PageTest.login(), R.url_of_a_business);
        assertCorrectPage(business_page);
        Business a_business = business_page.convert_to_business();
        assertNotNull(a_business.open_collection_dates());

        business_page = Get.business_page(business_page, R.url_of_another_business);
        assertCorrectPage(business_page);
        Business another_business = business_page.convert_to_business();
        assertNotNull(another_business.open_collection_dates());

        map = new BusinessesAndDates();
        map.add(a_business);
        map.add(another_business);
    }

    @Test
    public void print_to_string() {
        System.out.println(Print.toString(map));
    }

    @Test
    public void print_to_forum_thread() {
        System.out.println(Print.overview_of_open(map));
    }
}