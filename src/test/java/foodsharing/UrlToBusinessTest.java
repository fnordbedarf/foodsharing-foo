package foodsharing;

import foodsharingpage.*;
import helper.*;
import model.*;
import org.junit.*;

import static org.junit.Assert.*;

public class UrlToBusinessTest extends PageTest{

    private BusinessPage businessPage;

    @Before
    public void before() {
        businessPage = Get.business_page(PageTest.login(), R.url_of_a_business);
        assertCorrectPage(businessPage);
    }

    @Test
    public void get_a_business() {
        Business business = businessPage.convert_to_business();
        assertNotNull(business.name());
        assertNotNull(business.url());
        assertFalse(business.open_collection_dates().isEmpty());
    }
}