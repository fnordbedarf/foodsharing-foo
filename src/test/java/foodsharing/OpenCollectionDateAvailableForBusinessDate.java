package foodsharing;

import foodsharingpage.*;
import helper.*;
import org.junit.*;

import static org.junit.Assert.*;

public class OpenCollectionDateAvailableForBusinessDate extends PageTest {


    @Test
    public void message_to_team() {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);

        BusinessPage businessPage = Get.business_page(dashboard, R.url_of_a_business);
        assertCorrectPage(businessPage);

        boolean has_open_collection_date = businessPage.has_open_collection_date_in_next_three_days();

        close(businessPage);

        assertFalse(has_open_collection_date);
    }
}
