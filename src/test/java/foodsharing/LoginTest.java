package foodsharing;

import foodsharingpage.*;
import helper.*;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.support.*;

import java.util.*;

import static org.junit.Assert.*;

public class LoginTest extends PageTest{

    private static WebDriver driver;

    @After
    public void after () {
        close(driver);
    }

    @Test
    public void login_to_foodsharing_version_2() {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);
    }

    @Test
    public void login_to_foodsharing_version_1() {
        driver = Get.driver(R.url);

        LoginPage login = PageFactory.initElements(driver, LoginPage.class);
        assertEquals("foodsharing | Rette mit!", login.getTitle());

        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = PageFactory.initElements(driver, DashBoardPage.class);
        assertEquals("foodsharing | Dashboard", dashboard.getTitle());
    }

    @Test
    public void login_to_foodsharing() {
        String path = "/home/sammann/git/foodsharing-foo/src/test/resources/geckodriver";
        System.setProperty("webdriver.gecko.driver", path);
        driver = new FirefoxDriver();
        String url = "https://foodsharing.de/";
        driver.get(url);
        LoginPage login = PageFactory.initElements(driver, LoginPage.class);
        assertEquals("foodsharing | Rette mit!", login.getTitle());
        ResourceBundle properties = ResourceBundle.getBundle("resource");
        String email = properties.getString("email");
        String password = properties.getString("password");
        login.with_credentials(email, password);
        DashBoardPage dashboard = PageFactory.initElements(driver, DashBoardPage.class);
        assertEquals("foodsharing | Dashboard", dashboard.getTitle());
    }
}