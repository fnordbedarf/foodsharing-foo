package foodsharing;

import foodsharingpage.*;
import helper.*;
import model.*;
import org.junit.*;

public class GetTodaysCollector extends PageTest {

    @Test
    public void test() {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        System.out.println("::: LoginPage loaded.");
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);
        System.out.println("::: DashBoard loaded.");

        BusinessPage businessPage = Get.business_page(dashboard, R.url_of_a_business);
        assertCorrectPage(businessPage);
        System.out.println("::: BusinessPage loaded.");

        Business business = businessPage.convert_to_business();
        String message = business.get_today_s_collector();
        System.out.println(message);
    }
}