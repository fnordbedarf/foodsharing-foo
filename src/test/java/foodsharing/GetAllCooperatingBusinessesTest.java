package foodsharing;

import foodsharingpage.*;
import helper.*;
import org.junit.*;

import java.util.*;

import static org.junit.Assert.*;

public class GetAllCooperatingBusinessesTest extends PageTest{

    private static AllBusinessesPage all_businesses;

    @Before
    public void before() {
        LoginPage login = Get.login_page();
        assertCorrectPage(login);
        login.with_credentials(R.email, R.password);

        DashBoardPage dashboard = Get.dashboard_page(login);
        assertCorrectPage(dashboard);
        dashboard.get(R.url_of_all_businesses_page);

        all_businesses = Get.all_businesses_page(dashboard);
    }

    @Test
    public void get_all_businesses_urls() {
        List<String> urls = all_businesses.in_cooperation_urls();
        for (String url : urls) {
            System.out.println(url);
        }
    }
}