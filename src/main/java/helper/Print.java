package helper;

import model.*;

import java.util.*;

public class Print { //TODO this class is obsolete as "Print", as it converts objects to return strings and not print sys out anymore

    public static String toString(BusinessesAndDates map) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<CollectionDate, Businesses> entry : map.entrySet()) {
            builder.append(To.string_convert(entry.getKey().calendar()))
                    .append(" ")
                    .append(names_of(entry.getValue()))
                    .append("\n");
        }
        return builder.toString();
    }

    private static String names_of(Businesses businesses) {
        StringBuilder builder = new StringBuilder();
        for (Business business : businesses) {
            builder.append(business.name() + ", ");
        }
        return trimmed(builder);
    }

    private static String trimmed(StringBuilder builder) {
        return builder.toString().trim().replaceAll(", $", "");
    }

    public static String overview_of_open(BusinessesAndDates business_and_dates) {
        StringBuilder overview = new StringBuilder();
        Calendar in_three_days = Calendar.getInstance();
        in_three_days.add(Calendar.DATE, 3);

        for (Map.Entry<CollectionDate, Businesses> entry : business_and_dates.entrySet()) {
            if (entry.getKey().calendar().before(in_three_days)) {
                overview.append(markdown_of(entry));
            }
        }
        return overview.toString();
    }

    private static String markdown_of(Map.Entry<CollectionDate, Businesses> entry) {
        return new StringBuilder()
                .append(To.human_readable_string_convert(entry.getKey().calendar()))
                .append(" ")
                .append(markdown_of_names_of(entry.getValue()))
                .append("\n")
                .toString();
    }

    private static String markdown_of_names_of(Businesses businesses) {
        StringBuilder builder = new StringBuilder();
        for (Business business : businesses) {
            builder.append(markdown_of_(business));
        }
        return trimmed(builder);
    }

    private static String markdown_of_(Business business) {
        return "[" + business.name() + "](" + business.url() + "), ";
    }

    public static String visited_businesses(BusinessesAndDates businessesAndDates) {
        StringBuilder builder = new StringBuilder("Besuchte Betriebe: ");
        for(Business business : businessesAndDates.businesses()) {
            builder.append(markdown_of_(business)).append(", ");
        }
        return trimmed(builder);
    }

    public static String message_to_team_to_sign_up() {
        Calendar cal = Calendar.getInstance();
        int day_of_month = cal.get(Calendar.DAY_OF_MONTH);
        return R.message_of(day_of_month);
    }
}