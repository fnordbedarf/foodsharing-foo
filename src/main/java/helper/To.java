package helper;

import org.json.simple.*;
import org.json.simple.parser.*;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.*;

import java.io.*;
import java.text.*;
import java.util.*;

public class To {

    private static final JSONParser parser = new JSONParser();
    private static final SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat readable_dateformat = new SimpleDateFormat("E'., den' dd.MM.yy 'um' HH:mm", Locale.GERMANY);

    public static JSONObject json(String key, String[] strings) {
        JSONArray businesses_urls = new JSONArray();
        businesses_urls.addAll(Arrays.asList(strings));
        JSONObject object = new JSONObject();
        object.put(key, businesses_urls);
        return object;
    }

    public static File file(String filename, JSONObject object) {
        File file = new File(filename);
        try (FileWriter to_file = new FileWriter(file)) {
            to_file.write(object.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static JSONObject json(String filename) throws IOException, ParseException {
        return (JSONObject) parser.parse(new FileReader(filename));
    }

    public static Calendar calendar_convert(String timestamp) throws java.text.ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateformat.parse(timestamp));
        return calendar;
    }

    public static String string_convert(Calendar calendar) {
        return dateformat.format(calendar.getTime());
    }

    public static String human_readable_string_convert(Calendar calendar) {
        return readable_dateformat.format(calendar.getTime());
    }

    public static void string(WebDriverException exception) {
        WrappedWebDriverException wrapper = new WrappedWebDriverException(exception);
        System.out.println(wrapper);
    }
}