package helper;

import foodsharingpage.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.support.*;
import pageobject.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class Get {

    private static final String path_to_project = new File("").getAbsolutePath();

    private static WebDriver driver() {
        FirefoxBinary binary = new FirefoxBinary();
        add_command_line_options_to_(binary);
        System.setProperty("webdriver.gecko.driver", R.path_to_webdriver_gecko_driver);
        FirefoxOptions options = new FirefoxOptions();
        //options.setLogLevel(FirefoxDriverLogLevel.TRACE); TODO logging
        options.setBinary(binary);
        return Get.driver(options);
    }

    private static WebDriver driver(FirefoxOptions options) {
        return new FirefoxDriver(options);
    }

    private static void add_command_line_options_to_(FirefoxBinary binary) {
        String options = System.getProperty("headless");
        if (options != null && ! options.equals("null")) {
            binary.addCommandLineOptions(options);
        }
    }

    public static WebDriver driver(String url) {
        WebDriver driver = driver();
        driver.get(url);
        return driver;
    }

    public static LoginPage login_page() {
        return page_from(Get.driver(R.url), LoginPage.class);
    }

    public static DashBoardPage dashboard_page(PageObject pageObject) {
        return page_from(pageObject.driver(), DashBoardPage.class);
    }

    public static AllBusinessesPage all_businesses_page(PageObject pageObject) {
        return page_from(pageObject.driver(), AllBusinessesPage.class);
    }

    public static BusinessPage business_page(PageObject pageObject, String url) {
        pageObject.driver().get(url);
        return page_from(pageObject.driver(), BusinessPage.class);
    }

    public static ThreadPage threadpage(String url, PageObject pageObject) {
        pageObject.driver().get(url);
        return page_from(pageObject.driver(), ThreadPage.class);
    }

    private static <T> T page_from(WebDriver driver, Class<T> pageClassToProxy) {
        return PageFactory.initElements(driver, pageClassToProxy);
    }

    public static ThreadPage threadpage(PageObject pageObject) {
        return threadpage(R.url_of_thread, pageObject);
    }

    public static String timestamp() {
        Date date = Calendar.getInstance().getTime();
        long time = date.getTime();
        return new Timestamp(time).toString();
    }

    public static String path_to(String path) {
        return path_to_project + path;
    }
}