package helper;

import java.util.*;

public class R {

    // resource.properties
    private static final ResourceBundle resource = ResourceBundle.getBundle("resource");
    public static final String url = get("url");
    public static final Long timeout_in_seconds = Long.valueOf(get("timeout_in_seconds"));
    public static final Long sleep_in_millis = Long.valueOf(get("sleep_in_millis"));
    public static final Long page_time_to_load_in_millis = Long.valueOf(get("page_time_to_load_in_millis"));
    public static final String url_of_all_businesses_page = get("url_of_all_businesses_page");
    public static final String url_of_a_business = get("url_of_a_business");
    public static final String url_of_another_business = get("url_of_another_business");
    public static final String url_of_thread = get("url_of_thread");
    public static final String path_to_businesses_url_file = get_path_to("path_to_businesses_url_file");
    public static final String path_to_businesses_url_to_confirm_collectors_file = get_path_to("path_to_businesses_url_to_confirm_collectors_file");
    public static final String path_to_webdriver_gecko_driver = get_path_to("path_to_webdriver_gecko_driver");
    public static final String path_to_businesses_to_be_messages_url_file = get_path_to("path_to_businesses_to_be_messages_url_file");
    private static String get(String key) {
        return resource.getString(key);
    }
    private static final String get_path_to(String key) {
        return Get.path_to(get(key));
    }

    // credentials.properties
    private static final ResourceBundle credentials = ResourceBundle.getBundle("credentials");
    public static final String email = credentials.getString("email");
    public static final String password = credentials.getString("password");
    public static final String bottoken = credentials.getString("bottoken");

    // messages.properties
    private static final ResourceBundle messages = ResourceBundle.getBundle("messages", new UTF8Control());
    public static String message_of(int i){
        String key = "message" + String.format("%02d", i);
        return messages.getString(key);
    }
}