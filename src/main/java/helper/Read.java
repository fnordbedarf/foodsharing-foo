package helper;

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

public class Read {

    public static List<String> urls_from(String filename) throws IOException {
        List<String> urls = new LinkedList<>();
        Stream<String> lines = Files.lines(Paths.get(filename), Charset.forName("UTF-8"))/*.filter(line -> line.startsWith("https"))*/;
        lines.forEach(urls :: add);
        lines.close();
        return urls;
    }
}
