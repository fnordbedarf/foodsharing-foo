package helper;

import org.openqa.selenium.*;

public class WrappedWebDriverException extends WebDriverException {

    private final WebDriverException exception;

    public WrappedWebDriverException(WebDriverException exception) {
        this.exception = exception;
    }

    @Override
    public String toString() {
        return new StringBuilder("\n\n")
                .append("getAdditionalInformation: " + exception.getAdditionalInformation() + "\n\n")
                .append("getSystemInformation " + exception.getSystemInformation() + "\n\n")
                .toString();
    }
}
