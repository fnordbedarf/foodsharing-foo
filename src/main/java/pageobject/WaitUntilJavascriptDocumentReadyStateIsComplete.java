package pageobject;

public class WaitUntilJavascriptDocumentReadyStateIsComplete extends WaitUntilCondition {

    @Override
    protected void execute_javascript() {
        execute("return document.readyState");
    }

    @Override
    protected Boolean evaluate_result() {
        return result().toString().equals("complete");
    }
}
