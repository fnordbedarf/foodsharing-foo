package pageobject;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;

abstract class WaitUntilCondition implements ExpectedCondition<Boolean> {

    private JavascriptExecutor javascript;
    private Object result_of_execution;
    private Boolean isWaitOver;

    public Boolean apply(final WebDriver driver) {
        javascript = (JavascriptExecutor) driver;
        execute_javascript_to_check_if_wait_is_over();
        return isWaitOver;
    }

    private void execute_javascript_to_check_if_wait_is_over() {
        try {
            isWaitOver = execute_javascript_and_evaluate_result();
        } catch (final Exception javascriptNotRunning) {
            isWaitOver = Boolean.TRUE;
        }
    }

    private Boolean execute_javascript_and_evaluate_result() {
        execute_javascript();
        return evaluate_result();
    }

    protected abstract void execute_javascript();

    protected abstract Boolean evaluate_result();

    void execute(final String code) {
        result_of_execution = javascript.executeScript(code);
    }

    Object result() {
        return result_of_execution;
    }

}