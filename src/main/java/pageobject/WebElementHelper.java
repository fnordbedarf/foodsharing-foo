package pageobject;

import org.openqa.selenium.*;

public class WebElementHelper {

    private final WebElement element;

    WebElementHelper(WebElement element) {
        this.element = element;
    }

    public void write(String message) {
        element.sendKeys(message);
    }

    public void write(Keys keys) {
        element.sendKeys(keys);
    }
}
