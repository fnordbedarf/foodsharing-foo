package pageobject;

import helper.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;

import java.util.*;

public abstract class PageObject {

    private final WebDriver driver;
    private final Wait<WebDriver> wait;
    private final WaitUntilCondition jQueryIsLoaded = new WaitUntilJQueryIsNotActiveAnymore();
    private final WaitUntilCondition javascriptIsLoaded = new WaitUntilJavascriptDocumentReadyStateIsComplete();

    public PageObject(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, R.timeout_in_seconds, R.sleep_in_millis);
    }

    public String getTitle() {
        return driver.getTitle();
    }

    protected Boolean wait_until(ExpectedCondition<Boolean> isTrue) {
        give_page_time_to_load();
        return wait.until(isTrue);
    }

    protected void wait_until_number_of(ExpectedCondition<List<WebElement>> elements_to_be) {
        give_page_time_to_load();
        wait.until(elements_to_be);
    }

    public boolean wait_until_loaded() {
        return wait_until(ExpectedConditions.invisibilityOfElementLocated(By.id("fancybox-loading")))
                && wait_until(ExpectedConditions.titleIs(title()))
                && wait_until(jQueryIsLoaded)
                && wait_until(javascriptIsLoaded);
    }

    protected abstract String title();

    private void give_page_time_to_load() {
        this.give_page_time_to_load(R.page_time_to_load_in_millis);
    }

    public void give_page_time_to_load(long page_time_to_load_in_millis) {
        try {
            Thread.sleep(page_time_to_load_in_millis);
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
        }
    }

    protected WebElementHelper into(WebElement element) {
        wait_until_loaded();
        return new WebElementHelper(element);
    }

    protected void click_on(WebElement element) {
        wait_until_loaded();
        element.click(/*do not click and assume verify to pass after click triggers a page load, ie.*/);
    }

    public WebDriver driver() {
        return driver;
    }

    public boolean verify() {
        wait_until_loaded();
        return verifying();
    }

    protected abstract boolean verifying();

    public void get(String url) {
        driver.get(url);
    }

    protected boolean expected_title() {
        return getTitle().equals(title());
    }

    protected String current_url() {
        return driver().getCurrentUrl();
    }

    protected void click_on(List<WebElement> elements) {
        for (WebElement element : elements) {
            try {
                click_on(element);
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }

    public void close() {
        driver.close();
    }
}