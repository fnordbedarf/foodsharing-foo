package pageobject;

public class  WaitUntilJQueryIsNotActiveAnymore<B> extends WaitUntilCondition  {

    @Override
    protected void execute_javascript() {
        execute("return jQuery.active");
    }

    @Override
    protected Boolean evaluate_result() {
        final Long result = Long.valueOf((String) result());
        return result == Long.valueOf(0);
    }
}
