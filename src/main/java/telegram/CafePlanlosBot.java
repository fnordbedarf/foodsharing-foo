package telegram;

import foodsharingpage.*;
import helper.*;
import model.*;
import org.telegram.telegrambots.*;
import org.telegram.telegrambots.bots.*;
import org.telegram.telegrambots.meta.*;
import org.telegram.telegrambots.meta.api.methods.send.*;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.exceptions.*;

import java.time.*;
import java.util.*;

public class CafePlanlosBot extends TelegramLongPollingBot {

    private String today_s_collector;
    private LocalDateTime timestamp;

    @Override
    public void onUpdateReceived(Update update) {
        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            String text = create_message_from(update.getMessage().getText());
            try {
                if ( ! text.isEmpty()) {
                    SendMessage message = new SendMessage() // Create a SendMessage object with mandatory fields
                            .setChatId(update.getMessage().getChatId())
                            .setText(text);
                    execute(message); // Call method to send the message
                }
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    private String create_message_from(String text) {
        if (text.contains("/werholtheuteab")) {
            if (today_s_collector == null || timestamp_too_old() ) {
                set_timestamp();
                today_s_collector = get_today_s_collector();
            }
            text = today_s_collector;
        } else {
            text = "";
        }
        return text;
    }

    private void set_timestamp() {
        Instant now = Instant.now();
        TimeZone tz = Calendar.getInstance().getTimeZone();
        ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
        timestamp = LocalDateTime.ofInstant(now, zid);
    }

    private boolean timestamp_too_old() {
        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now(ZoneId.of("Europe/Berlin"));
        LocalDateTime today_midnight = LocalDateTime.of(today, midnight);
        return today_midnight.isAfter(timestamp);
    }

    private String get_today_s_collector() {
        LoginPage login = Get.login_page();
        login.verify();
        System.out.println("::: LoginPage loaded.");

        login.with_credentials(R.email, R.password);
        DashBoardPage dashboard = Get.dashboard_page(login);
        dashboard.verify();
        System.out.println("::: DashBoard loaded.");

        BusinessPage businessPage = Get.business_page(dashboard, R.url_of_a_business);
        businessPage.verify();
        System.out.println("::: BusinessPage loaded.");

        Business business = businessPage.convert_to_business();
        businessPage.close();
        return business.get_today_s_collector();
    }

    @Override
    public String getBotUsername() {
        return "cafeplanlosbot";
    }

    @Override
    public String getBotToken() {
        return R.bottoken;
    }
}