package model;

import helper.*;

import java.util.*;

public class BusinessesAndDates {

    private final Businesses businesses;
    private final CollectionDateBusinessesMap open_collection_dates;

    public BusinessesAndDates() {
        open_collection_dates = new CollectionDateBusinessesMap();
        businesses = new Businesses();
    }

    public void add(Business business) {
        businesses.add(business);
        for (CollectionDate open_collection_date : business.open_collection_dates()) {
            open_collection_dates.put(open_collection_date, business);
        }
    }

    public int size() {
        return open_collection_dates.size();
    }

    @Override
    public String toString() {
        return Print.toString(this);
    }

    public String to_forum_thread () {
        return new StringBuilder()
                .append(Print.overview_of_open(this))
                .append("\n")
                .append(Print.visited_businesses(this))
                .toString();
    }

    public Set<Map.Entry<CollectionDate, Businesses>> entrySet() {
        return open_collection_dates.entrySet();
    }

    public Businesses businesses() {
        return businesses;
    }
}