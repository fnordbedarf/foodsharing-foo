package model;

import java.util.*;

public class CollectionDateBusinessesMap {

    private final TreeMap<CollectionDate, Businesses> map;

    public CollectionDateBusinessesMap() {
        map = new TreeMap<>();
    }

    public void put(CollectionDate collection_date, Business business) {
        boolean contains_key = map.containsKey(collection_date);
        if (contains_key) {
            map.get(collection_date).add(business);
        } else {
            Businesses businesses = new Businesses();
            businesses.add(business);
            map.put(collection_date, businesses);
        }
    }

    public int size() {
        return map.size();
    }

    public Set<Map.Entry<CollectionDate, Businesses>> entrySet() {
        return map.entrySet();
    }
}
