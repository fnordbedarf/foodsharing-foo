package model;

import java.util.*;

public class Business implements Comparable<Business>{

    private final String name;
    private final String url;
    private final List<CollectionDate> open_collection_dates;
    private final String today_s_collector;

    public Business(String name, String current_url, List<CollectionDate> open_collection_dates, String today_s_collector) {
        this.name = name;
        this.url = current_url;
        this.open_collection_dates = open_collection_dates;
        this.today_s_collector = today_s_collector;
    }

    @Override
    public String toString() {
        return this.url + "\n" + open_collection_dates;
    }

    public String url() {
        return url;
    }

    public String name() {
        return name;
    }

    public List<CollectionDate> open_collection_dates() {
        return open_collection_dates;
    }


    @Override
    public int compareTo(Business business) {
        return name.compareTo(business.name);
    }

    public boolean has_open_collections() {
        boolean has_open_collections = false;
        if (open_collection_dates != null && open_collection_dates.isEmpty()) {
            has_open_collections = true;
        }
        return has_open_collections;
    }

    public String get_today_s_collector() {
        return today_s_collector;
    }
}