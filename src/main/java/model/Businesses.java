package model;

import java.util.*;

public class Businesses implements Iterable<Business> {

    private final Set<Business> businesses;

    public Businesses() {
        this.businesses = new TreeSet<>();
    }

    public void add(Business business) {
        businesses.add(business);
    }

    @Override
    public Iterator<Business> iterator() {
        return businesses.iterator();
    }
}
