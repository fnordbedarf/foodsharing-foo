package model;

import helper.*;

import java.util.*;

public class CollectionDate implements Comparable<CollectionDate> {

    private final Calendar calendar;

    public CollectionDate(Calendar calendar) {
        this.calendar = calendar;
    }

    @Override
    public int compareTo(CollectionDate collection_date) {
        return calendar.compareTo(collection_date.calendar);
    }

    @Override
    public String toString() {
        return To.string_convert(calendar);
    }

    public Calendar calendar() {
        return calendar;
    }
}