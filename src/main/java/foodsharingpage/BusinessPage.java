package foodsharingpage;

import helper.*;
import model.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.*;
import pageobject.*;

import java.text.*;
import java.util.*;

public class BusinessPage extends PageObject {

    @FindBy (how = How.XPATH, using = "//div[contains(@id, 'fetch-')]")
    public  List<WebElement> collection_date_boxes;

    @FindBy (how = How.XPATH, using = "//a[@class='context-unconfirmed']")
    private List<WebElement> unconfirmed_collectors;

    @FindBy (how = How.XPATH, using = "(//li[@class='context-menu-item context-menu-icon context-menu-icon--fa5'])[2]")
    private List<WebElement> confirm;

    @FindBy (how = How.XPATH, using = "(//div[@id='vmenu']/ul[@class='linklist']/li/a)[1]")
    private WebElement message_to_team;

    @FindBy (how = How.XPATH, using = "//textarea[@class=\"chatboxtextarea\"]")
    private WebElement chatbox;

    @FindBy (how = How.XPATH, using = "(//div[contains(@id, 'fetch-')])[1]//img")
    private WebElement today_s_collector_img;

    public BusinessPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String title() {
        return getTitle(); // title cannot be set for all different businesses
    }

    @Override
    protected boolean verifying() {
        return getTitle().startsWith("foodsharing | Kooperationsbetrieb | ");
    }

    public Business convert_to_business() {
        String name = name_of(getTitle());
        List<CollectionDate> open_collection_date = open_collection_date_boxes();
        String today_s_collector = today_s_collector();
        return new Business(name, current_url(), open_collection_date, today_s_collector);
    }

    private String today_s_collector() {
        String collector;
        try {
            collector = today_s_collector_img.getAttribute("alt");
        } catch (Exception e) {
            collector = "Niemand.";
        }
        return collector;
    }

    private String name_of(String title) {
        String[] words = title.split("\\|");
        return words[2].trim();
    }

    private List<CollectionDate> open_collection_date_boxes() {
        List<CollectionDate> keep = new LinkedList<>();
        for (WebElement box : collection_date_boxes) {
            if (still_open(box)) {
                WebElement value = box.findElement(By.tagName("input"));
                String timestamp = value.getAttribute("value");
                try {
                    Calendar calendar = To.calendar_convert(timestamp);
                    keep.add(new CollectionDate(calendar));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return keep;
    }

    private boolean still_open(WebElement box) {
        boolean still_open;
        try {
            String imagename = box.findElement(By.tagName("img")).getAttribute("src");
            still_open = imagename.contains("nobody.gif");
        } catch (Exception element_not_found_or_so) {
            still_open = false;
            System.err.println(current_url());
        }
        return still_open;
    }

    public void confirm_all_collectors() {
        for (WebElement unconfirmed_collector : unconfirmed_collectors) {
            click_on(unconfirmed_collector);
            click_on(confirm);
        }
        wait_until_loaded();
    }

    public void message_to_team(String message) {
        click_on(message_to_team);
        click_on(chatbox);
        into(chatbox).write(message);
        into(chatbox).write(Keys.ENTER);
    }

    public boolean has_open_collection_date_in_next_three_days() {
        boolean has_open_collection_dates = false;
        List<CollectionDate> open_collection_dates = open_collection_date_boxes();
        Calendar in_three_days = Calendar.getInstance();
        in_three_days.add(Calendar.DATE, 3);
        for (CollectionDate open_collection_date :  open_collection_dates) {
            Calendar collection_date = open_collection_date.calendar();
            if(collection_date.before(in_three_days)) {
                has_open_collection_dates = true;
                break;
            }
        }
        return has_open_collection_dates;
    }

    public void message_to_team_if_open_collections_exist() {
        if (has_open_collection_date_in_next_three_days()) {
            String to_sign_up = Print.message_to_team_to_sign_up();
            message_to_team(to_sign_up);
        }
    }
}