package foodsharingpage;

import org.openqa.selenium.*;
import pageobject.*;

public class DashBoardPage extends PageObject {

    public DashBoardPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String title() {
        return "foodsharing | Dashboard";
    }

    @Override
    protected boolean verifying() {
        return expected_title();
    }
}