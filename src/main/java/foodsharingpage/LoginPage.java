package foodsharingpage;

import org.openqa.selenium.*;
import org.openqa.selenium.support.*;
import pageobject.*;

public class LoginPage extends PageObject {

    @FindBy ( id = "login-email" )
    private WebElement email;

    @FindBy ( id = "login-password" )
    private WebElement password;

    @FindBy ( css = ".btn" )
    private WebElement submit;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String title() {
        return "foodsharing | Rette mit!";
    }

    @Override
    protected boolean verifying() {
        return expected_title();
    }

    public void with_credentials(String _email, String _password) {
        into(email).write(_email);
        into(password).write(_password);
        click_on(submit);
    }
}