package foodsharingpage;

import org.openqa.selenium.*;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.*;
import pageobject.*;

import java.util.*;

public class ThreadPage extends PageObject  {

    @FindBy ( xpath = "//textarea")
    private WebElement textarea;

    @FindBy ( xpath = "//button[@class='btn btn-secondary']" )
    private List<WebElement> confirm_message;

    @FindBy ( xpath = "//footer[@id='__BVID__87___BV_modal_footer_']" )
    private WebElement delete_dialog;

    private By by_trash_icon = By.xpath("//i[@class = 'fas fa-trash-alt']");

    public ThreadPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String title() {
        return "foodsharing | Siegen | Forum";
    }

    @Override
    protected boolean verifying() {
        return expected_title();
    }

    /**
     * Brute force seems to be the best way.
     */
    public void delete_all_but_first_thread() {
        // Get all delete-icons...
        List<WebElement> delete = driver().findElements(by_trash_icon);
        // start with the second (1) one...
        for (int i = 1; i < delete.size(); i++ ) {
        // if the trash Icon is clickable (hick ups when script is too fast)
            if (delete.get(i).isDisplayed()) {
        // click on trash icon...
                click_on(delete.get(i));
        // then get all headers (one should have become visible after clicking the trash icon)
                List<WebElement> dialogs = driver().findElements(By.xpath("//header"));
        // for each header/delete-dialog (not all of them are displayed, but the one we need is)...
                for (int j = 0; j < dialogs.size(); j++) {
                    if (dialogs.get(j).isDisplayed()) {
        // if the dialog is displayed...
                        List<WebElement> confirms = dialogs.get(j).findElements(By.xpath("//button[@class='btn btn-primary']"));
        //
                        for (int k = 0; k < confirms.size(); k++) {
                            if (confirms.get(k).isDisplayed()) {
                                click_on(confirms.get(k));
                            }
                        }
                    }
                }
            }
        }
    }

    public void post(String message) {
        into(textarea).write(message);
        click_on(confirm_message);
        wait_until_loaded();
        wait_until_number_of(ExpectedConditions.numberOfElementsToBe(by_trash_icon, 2));
    }
}