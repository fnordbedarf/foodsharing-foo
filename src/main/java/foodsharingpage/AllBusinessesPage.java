package foodsharingpage;

import org.openqa.selenium.*;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.*;
import pageobject.*;

import java.util.*;

public class AllBusinessesPage extends PageObject {

    @FindBy ( id = "__BVID__49" )
    private WebElement status;

    @FindBy ( className = "page-item" )
    private List<WebElement> pages;

    @FindBy ( className = "ui-corner-all" )
    private List<WebElement> businesses;

    public AllBusinessesPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected String title() {
        return "foodsharing";
    }

    @Override
    public boolean verifying() {
        return expected_title();
    }

    public List<String> in_cooperation_urls() {
        Select cooperation  = new Select(status);
        cooperation.selectByVisibleText("In Kooperation");
        return urls_of_businesses_of_each_page();
    }

    private List<String> urls_of_businesses_of_each_page() {
        pages = keep_pagenumbers_in(pages);
        List<String> urls = new ArrayList<>();
        for (WebElement page : pages) {
            click_on(page);
            urls.addAll(href_of(businesses));
        }
        return urls;
    }

    private List<String> href_of(List<WebElement> businesses) {
        List<String> urls = new ArrayList<>();
        for (WebElement business : businesses) {
            String href = business.getAttribute("href");
            if (has_business_id(href)) {
                urls.add(href);
            }
        }
        return urls;
    }

    private boolean has_business_id(String href) {
        return href != null && href.contains("&id=");
    }

    private List<WebElement> keep_pagenumbers_in(List<WebElement> pages) {
        List<WebElement> keep = new LinkedList<>();
        for(WebElement page : pages) {
            String text = page.getText();
            try {
                Integer.parseInt(text);
                keep.add(page);
            } catch (Exception page_is_not_an_integer) {
                // do nothing;
            }
        }
        return keep;
    }
}