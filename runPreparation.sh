# Bash Options

set -e
set -u

# Alles killen, was vom vorigen ablauf vielleicht noch übrig geblieben ist.

pkill firefox-esr
pkill geckodriver
pkill selenium
pkill java