# foodsharing-foo

Diese Projekt beinhaltet Automatisierungsskripts für die Aufgaben eines BIEBs auf der foodsharing.de plattform.

Bei Fragen und Anregungen, entweder hier oder an sammann [at] fnordbedarf [punkt] de

## Anwendung

0. Installiere notwendige Software: 
`root $: apt-get install -y gradle openjava8-jdk firefox unzip xvfb libxi6 libgconf-2-4`.
1. Du hast ein Linux 64Bit (wegen `webdriver_gecko_driver`)
2. Clone dieses Repository lokal und füge es deinem Pfad hinzu. Zum Beispiel `export PATH=$PATH:~/foodsharing-foo/` in die `.profile`.
3. Passe `runPreparation.sh` an - siehe weiter unten
3. Gradle-Importiere es in deine IDE, zum Beispiel IntelliJ
4. Passe `src/test/resources/credentials.properties` an - siehe weiter unten
5. Passe `src/test/resources/resource.properties` an - siehe weiter unten
6. Passe `src/test/resources/businesses.txt` an - siehe weiter unten
6. Passe `src/test/resources/businesses_to_be_messaged.txt` an - siehe weiter unten
7. Passe `src/test/resources/message.sproperties` an - siehe weiter unten
8. Passe alle `runStatic` an, dass `crontab` in den Projektordner wechselt.
7. Ausführung
   1. Entweder du führst aus deiner IDE, zum Beispiel IntelliJ, die entsprechende Testklasse unter 
   `src/test/java/automation/` aus.
   2. Oder du führst ein entsprechendes `run*` Skript aus der Bash-Shell aus
   3. Oder du nutzt `crontab -e`, eine Beispieldatei findet sich `src/test/reources/crontab.example`

### src/test/resources/credentials.properties

* Setzen musst du auf jeden Fall:
  * `username`
  * `password`
Achtung: Du solltest diese Datei nicht hochladen.

### src/test/resources/resource.properties

* Die Datei ist kommentiert. 
* Setzen musst du auf jeden Fall:
  * `url_of_thread` Du musst auf der Foodsharing-Plattform in deinem gewünschten Forum einen Thread erstellen. Die URL 
  des Threads muss hier hin, damit bei der Automatisierung hier reingepostet wird.
  * `webdriver_gecko_driver` den Pfad musst du anpassen, er ist in diesem Projekt `src/resources/geckodriver`
Der Rest sollte passen.
 
### src/test/resources/businesses.txt

* Je Zeile eine URL eines Betriebes, der besucht werden soll. 
* Das macht das `StaticOpenCollectionsThreeDaysPostTest`-Skript `...Static...`, weil die URLs der Betrieben 
statisch von der Datei `src/test/resources/businesses.txt` eingelesen werden. 
* Wenn du einmal alle kooperierende Betriebe deines Bezirks eingelesen und auf Kommandozeile ausgegeben haben möchtest, 
dann setze `url_of_all_businesses_page` in der `resource.properties` und führe 
`src/test/java/foodsharing/GetAllCooperatingBusinessesTest.java` aus.

### src/test/resources/businesses_to_be_messaged.txt

* Je Zeile eine URL eines Betriebes, der besucht werden soll. 
* Das macht das `StaticMessageToTeamIfOpenCollectionTest`-Skript `...Static...`, weil die URLs der Betrieben 
statisch von der Datei `src/test/resources/businesses_to_be_messaged.txt` eingelesen werden. 

### src/test/resources/message.sproperties

* Es sind 31 Einträge notwendig, weil der längste Monat 31 Tage hat. 
* Zwei-stellige Zahl am Ende von "message"
* Jetzt auch mit UTF-8
  
### runPreparation

* Die Datei ist kommentiert.
* Vor Allem ist der Pfad zum Projekt anzupassen.
  
  
## Implementierte Automation

### `StaticOpenCollectionsThreeDaysPostTest`

* Dieses Skript sollte täglich morgens laufen. Es zieht sich eine statische Liste an URLS von Betrieben 
(`...Static...`) aus der Textdatei `businesses_confirm_collectors.txt`. Die offenen Abholungen dieser Betriebe 
(`...OpenCollections...`) werden gesammelt und für die nächsten drei Tage (`...Three Days...`) in ein dafür  
eingerichteten Forumthread veröffentlicht (`...Post...`). 
* Dateiname: `src/test/java/automation/StaticOpenCollectionsThreeDaysPostTest`
* Es gibt ein Bash-Script `runStaticOpenCollectionsThreeDaysPostTest.sh`, es läuft im "Headless"-Modus, so dass du 
dort kein Browserfenster aufspringen siehst. Wenn du die Variable `-Dheadless="--headless"` entfernst, dann entfernst du
auch den "Headless"-Modus.

### `StaticConfirmCollectorTest`

* Dieses Skript sollte um 10, 12,14,16 und 17 Uhr laufen. Es zieht sich eine statische Liste an URLs von Betrieben
(`...Static...`) aus der Textdatei `businesses_confirm_collectors.txt`. Es besucht diese URLs und bestätigt 
(`...Confirm...`) alle noch unbestätigten Abholer (`...Collector...`).
* Dateiname: `src/test/java/automation/StaticConfirmCollectorTest.java`
* Es gibt ein Bash-Script `runStaticConfirmCollectorTest.sh`, es läuft im "Headless"-Modus, so dass du dort kein 
Browserfenster aufspringen siehst. Wenn du die Variable `-Dheadless="--headless"` entfernst, dann entfernst du auch den 
"Headless"-Modus.

### `StaticMessageToTeamIfOpenCollectionTest`

* Dieses Skript sollte einmal am Tag laufen. Es zieht sich eine statische Liste (`...Static...`) an URLs von Betrieben 
aus der Textdatei `businesses_to_be_messaged.txt` und schreibt dem Team des Betriebs (`...MessageToTeam...`), falls es 
offene Abholungen (`...IfOpenCollectionTest...`) existieren, eine Nachricht. Diese Nachricht ist vom Tag des Monats 
abhängig und liegt in `src/test/resources/messages.properties`.
* Dateiname: `src/test/resources/StaticMessageToTeamIfOpenCollectionTest.java`
* Es gibt ein Bash-Script `runStaticMessageToTeamIfOpenCollectionTest.sh`, es läuft im "Headless"-Modus, so dass du dort 
kein Browserfenster aufspringen siehst. Wenn du die Variable `-Dheadless="--headless"` entfernst, dann entfernst du auch 
den "Headless"-Modus.

### `CafePlanlosBotTest`
* Dies ist ein TelegramBot für das Cafe Planlos im Bezirk Siegen.
* `/werholtheuteab`: Beantwortet die Frage mit dem Namen des abholenden Foodsavers.

## Geplant

* Logging
* Testcoverage
* BIEB und Person im Betrieb per E-Mail oder Telegram-Gateway eine Stunde vor Abholung informieren, wenn sich keine ein-
getragen hat (Cornercase)
* Skript um die `src/test/resources/businesses.txt` zu aktualisieren.
* JUnit Ergebnis veröffentlichen
* Es gibt `TODO`s im Code verteilt
* move logging, reporting and test parameters to gradle, if possible
* rename Test methods uniformly

### Übergabe

Das Skript läuft jetzt seit Wochen und ich musste nichts nachsteuern. Das bedeutet, dass ich hier erstmal nichts mache, 
bis es wieder instabil ist. So ist das halt. Hier eine Liste von Dingen, die ich dann noch machen möchte
* Dokumentieren, wie JUnit auf dem Server die Dateien wegspeichert, damit sie von außen erreichbar sind
* Anstatt cron möchte ich systemd benutzen
* Fix: Der Telegram Bot aktualisiert im Laufe des Tages nicht, wenn Nobody zu $Foodsavername wird
* Wechsel der Anmeldedaten von e.shure auf bieb-dummy.
* Monatliches Update der kooperierenden Betriebe
